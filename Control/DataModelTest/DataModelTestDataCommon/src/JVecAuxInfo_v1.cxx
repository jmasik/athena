/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file DataModelTestDataCommon/JVecAuxInfo_v1.h
 * @author scott snyder <snyder@bnl.gov>
 * @date May, 2024
 * @brief For testing jagged vectors.
 */


#include "DataModelTestDataCommon/versions/JVecAuxInfo_v1.h"


namespace DMTest {


JVecAuxInfo_v1::JVecAuxInfo_v1()
  : xAOD::AuxInfoBase()
{
}


} // namespace DMTest

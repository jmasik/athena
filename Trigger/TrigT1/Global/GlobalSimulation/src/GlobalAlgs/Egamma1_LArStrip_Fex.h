/*
 *   Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
 */

#ifndef GLOBALSIM_EGAMMA1_LARSTRIP_FEX_H
#define GLOBALSIM_EGAMMA1_LARSTRIP_FEX_H

/*
  This Algorithm finds and outputs CaloCell in the neighborhoods of eFEX
  RoIs. These neighhoods are used to run various Algorithms in GlobalSim.
*/

#include "ICaloCellsProducer.h"
#include "eFexRoIAlgTool.h"

#include "../IO/LArStripNeighborhoodContainer.h"

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "xAODEventInfo/EventInfo.h"

#include <vector>

namespace GlobalSim {


  class Egamma1_LArStrip_Fex: public AthReentrantAlgorithm { 
  public:
    
    
    Egamma1_LArStrip_Fex(const std::string& name, ISvcLocator* pSvcLocator);

    virtual StatusCode  initialize() override;   
    virtual StatusCode  execute(const EventContext& ) const override;    

  private:

    SG::ReadHandleKey<xAOD::EventInfo> m_eventInfoKey{
      this,
	"EventInfo",
	"EventInfo",
	"EventInfo name"};
    
    // tool to get a vector of cal cells
    
    ToolHandle<ICaloCellsProducer> m_cellProducer{this,
	"caloCellProducer",
	"EMB1CellFromCaloCells",
	"AlgTool to provide a vector of CaloCells"
	};

    // tool to get eFexRoIs
    
    ToolHandle<eFexRoIAlgTool>
    m_roiAlgTool{this,
		 "roiAlgTool",
		 "EMB1CellFromCaloCells",
		 "AlgTool to provide a vector<const xAOD::eFexEMRoI*>"};

    Gaudi::Property<bool> m_dump {
      this,
      "dump",
      false,
      "flag to enable dumps"};

    Gaudi::Property<bool> m_dumpTerse {
      this,
      "dumpTerse",
      false,
      "flag to enable terse dumps"};

    SG::WriteHandleKey<LArStripNeighborhoodContainer>
    m_neighKey {
      this,
      "stripNeighborhoodKey",
      "stripNeighborhoodContainer",
      "location to write strip neighborhoods of EFex RoIs"};

    StatusCode
    findNeighborhoods(const std::vector<const xAOD::eFexEMRoI*>&,
		      const std::vector<const CaloCell*>&,
		      LArStripNeighborhoodContainer&) const;

    StatusCode
    findNeighborhood(const xAOD::eFexEMRoI*,
		     const std::vector<const CaloCell*>&,
		     LArStripNeighborhoodContainer&) const;

    StatusCode
    findClosestCellToRoI(const xAOD::eFexEMRoI*,
			 const std::vector<const CaloCell*>&,
			 const CaloCell*&) const;

    StatusCode
    dump(const xAOD::EventInfo& eventInfo,
	 const LArStripNeighborhoodContainer&) const;
    
    StatusCode
    dumpTerse(const xAOD::EventInfo& eventInfo,
	      const LArStripNeighborhoodContainer&) const;

		    
   
  };

}
#endif





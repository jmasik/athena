/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file    JsonPlotsDefReadTool.cxx
 * @author  Marco Aparo <marco.aparo@cern.ch>
 **/

/// Local include(s)
#include "JsonPlotsDefReadTool.h"
#include "SinglePlotDefinition.h"

/// Json parsing library
#include <nlohmann/json.hpp>


///--------------------------
///------- Initialize -------
///--------------------------
StatusCode IDTPM::JsonPlotsDefReadTool::initialize()
{
  ATH_MSG_INFO( "Initializing " << name() );
  ATH_CHECK( asg::AsgTool::initialize() );
  return StatusCode::SUCCESS;
}


///---------------------------
///--- getPlotsDefinitions ---
///---------------------------
std::vector< IDTPM::SinglePlotDefinition > 
IDTPM::JsonPlotsDefReadTool::getPlotsDefinitions() const
{
  using json = nlohmann::json;
  using cstr_t = const std::string&;
  using strVec_t = std::vector< std::string >;

  std::vector< SinglePlotDefinition > plotDefVec;

  /// Looping m_plotDefs for each histogram defnition string
  for( cstr_t plotDefStr : m_plotsDefs ) {

    ATH_MSG_DEBUG( "Reading plot definition : " << plotDefStr );

    /// parsing json format
    const json& plotDef = json::parse( plotDefStr );

    /// plot name
    cstr_t name = plotDef.contains( "name" ) ?
        plotDef.at( "name" ).get_ref< cstr_t >() : "";

    /// plot type
    cstr_t type = plotDef.contains( "type" ) ?
        plotDef.at( "type" ).get_ref< cstr_t >() : "";

    /// plot folder
    cstr_t folder = plotDef.contains( "folder" ) ?
        plotDef.at( "folder" ).get_ref< cstr_t >() : "";

    /// plot title
    cstr_t title = plotDef.contains( "title" ) ?
        plotDef.at( "title" ).get_ref< cstr_t >() : "";

    /// nBinsX
    unsigned int nBinsX = plotDef.contains( "xAxis_nBins" ) ?
        ( unsigned int )std::stoi(
            plotDef.at( "xAxis_nBins" ).get_ref< cstr_t >() ) : 0;

    /// nBinsY
    unsigned int nBinsY = plotDef.contains( "yAxis_nBins" ) ?
        ( unsigned int )std::stoi(
            plotDef.at( "yAxis_nBins" ).get_ref< cstr_t >() ) : 0;

    /// nBinsZ
    unsigned int nBinsZ = plotDef.contains( "zAxis_nBins" ) ?
        ( unsigned int )std::stoi(
            plotDef.at( "zAxis_nBins" ).get_ref< cstr_t >() ) : 0;

    /// xAxis limits
    float xLow = plotDef.contains( "xAxis_low" ) ?
        std::stof( plotDef.at( "xAxis_low" ).get_ref< cstr_t >() ) : 0.;
    float xHigh = plotDef.contains( "xAxis_high" ) ?
        std::stof( plotDef.at( "xAxis_high" ).get_ref< cstr_t >() ) : 0.;

    /// yAxis limits
    float yLow = plotDef.contains( "yAxis_low" ) ?
        std::stof( plotDef.at( "yAxis_low" ).get_ref< cstr_t >() ) : 0.;
    float yHigh = plotDef.contains( "yAxis_high" ) ?
        std::stof( plotDef.at( "yAxis_high" ).get_ref< cstr_t >() ) : 0.;

    /// zAxis limits
    float zLow = plotDef.contains( "zAxis_low" ) ?
        std::stof( plotDef.at( "zAxis_low" ).get_ref< cstr_t >() ) : 0.;
    float zHigh = plotDef.contains( "zAxis_high" ) ?
        std::stof( plotDef.at( "zAxis_high" ).get_ref< cstr_t >() ) : 0.;

    /// xAxis doLogLin
    bool xDoLogLinBins(false);
    if( plotDef.contains( "xAxis_doLogLinBins" ) ) {
      std::string xDoLogLinBinsStr = plotDef.at( "xAxis_doLogLinBins" ).get_ref< cstr_t >();
      if( xDoLogLinBinsStr == "true" ) xDoLogLinBins = true;
      else if( xDoLogLinBinsStr == "false" ) xDoLogLinBins = false;
      else ATH_MSG_WARNING( "xAxis_doLogLinBins not valid" );
    }

    /// yAxis doLogLin
    bool yDoLogLinBins(false);
    if( plotDef.contains( "yAxis_doLogLinBins" ) ) {
      std::string yDoLogLinBinsStr = plotDef.at( "yAxis_doLogLinBins" ).get_ref< cstr_t >();
      if( yDoLogLinBinsStr == "true" ) yDoLogLinBins = true;
      else if( yDoLogLinBinsStr == "false" ) yDoLogLinBins = false;
      else ATH_MSG_WARNING( "yAxis_doLogLinBins not valid" );
    }

    /// zAxis doLogLin
    bool zDoLogLinBins(false);
    if( plotDef.contains( "zAxis_doLogLinBins" ) ) {
      std::string zDoLogLinBinsStr = plotDef.at( "zAxis_doLogLinBins" ).get_ref< cstr_t >();
      if( zDoLogLinBinsStr == "true" ) zDoLogLinBins = true;
      else if( zDoLogLinBinsStr == "false" ) zDoLogLinBins = false;
      else ATH_MSG_WARNING( "zAxis_doLogLinBins not valid" );
    }

    /// xAxis bins (variable size)
    strVec_t xBinsStrVec;
    if( plotDef.contains( "xAxis_bins" ) ) xBinsStrVec = plotDef.at( "xAxis_bins" ).get< strVec_t >();
    std::vector< float > xBinsVec;
    for( cstr_t thisBin : xBinsStrVec ) xBinsVec.push_back( std::stof( thisBin ) );
    if( not xBinsVec.empty() ) {
      /// overwriting binning
      xLow = xBinsVec.front();  xHigh = xBinsVec.back();  nBinsX = xBinsVec.size() - 1;
    }

    /// yAxis bins (variable size)
    strVec_t yBinsStrVec;
    if( plotDef.contains( "yAxis_bins" ) ) yBinsStrVec = plotDef.at( "yAxis_bins" ).get< strVec_t >();
    std::vector< float > yBinsVec;
    for( cstr_t thisBin : yBinsStrVec ) yBinsVec.push_back( std::stof( thisBin ) );
    if( not yBinsVec.empty() ) {
      /// overwriting binning
      yLow = yBinsVec.front();  yHigh = yBinsVec.back();  nBinsY = yBinsVec.size() - 1;
    }

    /// zAxis bins (variable size)
    strVec_t zBinsStrVec;
    if( plotDef.contains( "zAxis_bins" ) ) zBinsStrVec = plotDef.at( "zAxis_bins" ).get< strVec_t >();
    std::vector< float > zBinsVec;
    for( cstr_t thisBin : zBinsStrVec ) zBinsVec.push_back( std::stof( thisBin ) );
    if( not zBinsVec.empty() ) {
      /// overwriting binning
      zLow = zBinsVec.front();  zHigh = zBinsVec.back();  nBinsZ = zBinsVec.size() - 1;
    }

    /// xTitle
    cstr_t xTitle = plotDef.contains( "xAxis_title" ) ?
        plotDef.at( "xAxis_title" ).get_ref< cstr_t >() : "";

    /// yTitle
    cstr_t yTitle = plotDef.contains( "yAxis_title" ) ?
        plotDef.at( "yAxis_title" ).get_ref< cstr_t >() : "";

    /// zTitle
    cstr_t zTitle = plotDef.contains( "zAxis_title" ) ?
        plotDef.at( "zAxis_title" ).get_ref< cstr_t >() : "";

    /// Adding new SinglePlotDefinition to vector
    plotDefVec.emplace_back(
        name, type, title,
        xTitle, nBinsX, xLow, xHigh, xDoLogLinBins, xBinsVec,
        yTitle, nBinsY, yLow, yHigh, yDoLogLinBins, yBinsVec,
        zTitle, nBinsZ, zLow, zHigh, zDoLogLinBins, zBinsVec,
        folder );

    /// Check if plot definition is valid. Removing
    if( not plotDefVec.back().isValid() ) {
      ATH_MSG_WARNING( "Tried to add invalid plot : " <<
                       plotDefVec.back().plotDigest() );
      plotDefVec.pop_back(); // removing from vector
    }

  } // close m_plotDefs loop

  return plotDefVec; 
}

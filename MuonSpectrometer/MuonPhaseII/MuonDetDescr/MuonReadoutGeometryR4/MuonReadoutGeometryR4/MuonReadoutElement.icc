/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONGEOMODELR4_MUONREADOUTELEMENT_ICC
#define MUONGEOMODELR4_MUONREADOUTELEMENT_ICC

#include <ActsGeoUtils/TransformCache.h>
namespace ActsTrk{
    template <> inline Amg::Transform3D 
        TransformCacheDetEle<MuonGMR4::MuonReadoutElement>::fetchTransform(const DetectorAlignStore* store) const {
        return m_parent->toStation(store);
   }
}

namespace MuonGMR4 {

inline const GeoAlignableTransform* MuonReadoutElement::alignableTransform() const { return m_args.alignTransform; }
inline Identifier MuonReadoutElement::identify() const { return m_args.detElId; }
inline IdentifierHash MuonReadoutElement::identHash() const { return m_detElHash;}
inline std::string MuonReadoutElement::chamberDesign() const { return m_args.chambDesign; }
inline const Muon::IMuonIdHelperSvc* MuonReadoutElement::idHelperSvc() const { return m_idHelperSvc.get(); }
inline Muon::MuonStationIndex::ChIndex MuonReadoutElement::chamberIndex() const { return m_chIdx; }
inline int MuonReadoutElement::stationName() const { return m_stName; }
inline int MuonReadoutElement::stationEta() const { return m_stEta; }
inline int MuonReadoutElement::stationPhi() const { return m_stPhi; }

/// Returns the detector center (Which is the same as the detector center of the
/// first measurement layer)
inline Amg::Vector3D MuonReadoutElement::center(const ActsGeometryContext& ctx) const {
    return localToGlobalTrans(ctx).translation();
}
inline Amg::Vector3D MuonReadoutElement::center(const ActsGeometryContext& ctx,
                                                const Identifier& id) const {
    return localToGlobalTrans(ctx, id).translation();
}
inline Amg::Vector3D MuonReadoutElement::center(const ActsGeometryContext& ctx, 
                                                const IdentifierHash& hash) const {
    return localToGlobalTrans(ctx, hash).translation();
}
inline Amg::Transform3D MuonReadoutElement::globalToLocalTrans(const ActsGeometryContext& ctx, 
                                                               const IdentifierHash& hash) const {   
    return localToGlobalTrans(ctx, hash).inverse();
}
inline Amg::Transform3D MuonReadoutElement::globalToLocalTrans(const ActsGeometryContext& ctx, 
                                                               const Identifier& id) const {
    return globalToLocalTrans(ctx, layerHash(id));
}
inline const Amg::Transform3D& MuonReadoutElement::localToGlobalTrans(const ActsGeometryContext& ctx, 
                                                                      const Identifier& id) const {
    return localToGlobalTrans(ctx, layerHash(id));
}

template<class MuonImpl>
StatusCode MuonReadoutElement::insertTransform(const IdentifierHash& hash) {    
    TransformCacheMap::const_iterator cache = m_localToGlobalCaches.find(hash);
    if (cache != m_localToGlobalCaches.end()) {
        ATH_MSG_FATAL(__FILE__<<":"<<__LINE__<<" - "<<idHelperSvc()->toStringDetEl(identify())
                   <<" has already a transformation cached for hash "<<hash);
        return StatusCode::FAILURE;
    }
    auto newCache = std::make_unique<ActsTrk::TransformCacheDetEle<MuonImpl>>(hash, static_cast<MuonImpl*>(this));
    m_localToGlobalCaches.insert(std::make_pair(hash, std::move(newCache)));
    return StatusCode::SUCCESS;
}

}  // namespace MuonGMR4
#endif

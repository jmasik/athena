/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../MeasurementMarkerAlg.h"
#include "../SegmentFitParDecorAlg.h"
#include "../SegmentMarkerAlg.h"
#include "../TruthMeasMarkerAlg.h"

DECLARE_COMPONENT(MuonR4::MeasurementMarkerAlg)
DECLARE_COMPONENT(MuonR4::SegmentFitParDecorAlg)
DECLARE_COMPONENT(MuonR4::SegmentMarkerAlg)
DECLARE_COMPONENT(MuonR4::TruthMeasMarkerAlg)
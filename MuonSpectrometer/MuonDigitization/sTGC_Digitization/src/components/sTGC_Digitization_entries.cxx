/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "sTGC_Digitization/sTgcDigitizationTool.h"
 
DECLARE_COMPONENT( sTgcDigitizationTool )
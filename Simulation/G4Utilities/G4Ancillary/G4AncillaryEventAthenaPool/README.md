# G4AncillaryEventAthenaPool
Author Thomas Kittelman (tkittel@mail.cern.ch)
Converted from packagedoc.h

## Introduction

This package creates a Pool converter for scintillator hit collections

## Class Overview

Pool converters are included for ScintillatorHitCollections and SimpleScintillatorHitCollections.
